<h1 align="center">More About Me  </h1>

![](/Images/ozil-Setup.jpg)

## My Favorite Youtube Channels : [![YouTube](https://img.icons8.com/color/48/000000/youtube--v1.png)](https://www.youtube.com/)
</br>
</br>
</br>

### *Tech & Cyber Security & Network & Linux & Programming & Gaming & Setup & Review Channels*

[![David Bombal](https://yt3.ggpht.com/ytc/AKedOLTVmtLFnPv-D9wjVmCdD7bSmVSARDsKBxFeXoh5=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/DavidBombal)
[![NetworkChuck](https://yt3.ggpht.com/ytc/AKedOLTHjL08GyIUwy_6DxA3GtUzyPU1aOs2CwgNtuRJ4A=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/NetworkChuck)
[![John Hammond](https://yt3.ggpht.com/ytc/AKedOLSKaXhsJjVcucr6ZGs4aFaBu_Qy-oY5z1KBLVGh0Q=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/JohnHammond010)
[![Hak5](https://yt3.ggpht.com/ytc/AKedOLQLYwUybycVx47iqpJ-AbSCPACIltHBWax5J8-Uzg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UC3s0BtrBJpwNDaflRSoiieQ)
[![ALFA Network Inc](https://yt3.ggpht.com/ytc/AKedOLQqCL-n54irXAaYTl_fAFJzeygWY-MrD4qN61Kg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/alfanetworkinc)
[![Chris Titus Tech](https://yt3.ggpht.com/ytc/AKedOLTE8sCvbu2Gebpjlu_bX4YJxwFbnKai7m8zxWD05g=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCg6gPGh8HU2U01vaFCAsvmQ)
[![TechHut](https://yt3.ggpht.com/TUoF-6QCUIKy6XgFtMG5FWi5FLVhtaUPtTOLvE7Ca3eJif1_RKBci07fKK-QvKxhC0HALEBH7Q=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/TechHutHD)
[![freeCodeCamp.org](https://yt3.ggpht.com/ytc/AKedOLTtJvQ1Vfew91vemeLaLdhjOwGx3tTBLlreK_QUyA=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/Freecodecamp)
[![Abdul Bari](https://yt3.ggpht.com/ytc/AKedOLQ0ZzmuKDUAnn9PnXylG707Oii6hd73U8rXbRGW=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCZCFT11CWBi3MHNlGf019nw)
[![Ghassan Shobaki Computer Science Lectures](https://yt3.ggpht.com/ytc/AKedOLRziOyQOebV9TKKzlhnrOtR0vtXCcbVg3x-YQ83=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCthr5rA6EA4c1S0L2OOiygA)
[![سنكرة SANKARA](https://yt3.ggpht.com/ytc/AKedOLQ341O5dYPL0sZVkhmaj22Sr7iNYMC33w1f3KFuUQ=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCNR623NFV3DbWpPTrA8cI_A)
[![Omardizer | عمرديزر](https://yt3.ggpht.com/ytc/AKedOLSHnYVS6_m1nE_EKdL8RqlADPkAc3uJh5CbUg1-gg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UC1FYFqgdW_1LueJTmXn-8cg)
[![Arabhardware - عرب هاردوير](https://yt3.ggpht.com/ytc/AKedOLSZPULXksuZCTCZKhEttms9Fyfl0AKbfQ7IRBnFtA=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UC4hJegNmN0gKiJMgVk3icHg)
[![Cambotar | كمبوطر ](https://yt3.ggpht.com/fsy0MPpnKZE2xBSrOJTbhtdOyBTGX0sq8gRmfE2CmXluga3WLI_FiIhyx4DMsyOXbQSUJEnQ=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UC-BZxIdlztx9bvptinxpnlg)
[![Linus Tech Tips](https://yt3.ggpht.com/ytc/AKedOLTK0y25C0RJiEngT1ae9mrZLXIortPD-DKQxOlRkw=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCXuqSBlHAE6Xw-yeJA0Tunw)
[![DIY Perks](https://yt3.ggpht.com/ytc/AKedOLS6UPNPQPwGeEU9cSybQQ9e3TiPAe7yyUu4eMcMRQ=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/DIYPerks)
[![Android Basha | أندرويد باشا](https://yt3.ggpht.com/gpNF-Hqc8iZJYgIxpRtWVB3K4-v0-yGjl5nxyDKmgS7UigeiW0V9M2T46D__j9ZNN1Xnc0G2=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/AndroidBasha)
[![Mustafa El-Feki - مصطفى الفقي](https://yt3.ggpht.com/5ZlPWfV6nIWIN9Lcc9mOdJgf514it0ir675qlUvnq3_awpBZyILPSQa_NwZcxFbaoUNaxrhyaA=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCIij97kWTAKuVPE1DBlZvaw)
[![ExplainingComputers](https://yt3.ggpht.com/U__DRmlaSCOXd6DLbo0luGSwy3QcWZ2qn3mKiopA_pGAc1-2IulmKBxf61WiAX5BLb-7nMylng8=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCbiGcwDWZjz05njNPrJU7jA)

###   *JetBrains Channels*

[![JetBrainsTV](https://yt3.ggpht.com/ytc/AKedOLTqWbbqTs-obiyFw5ZDvKGYXHnaSU6kY0ciqw0oFw=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/user/JetBrainsTV/channels)
[![IntelliJ IDEA by JetBrains](https://yt3.ggpht.com/ytc/AKedOLR7gbzR0xhCIkhUAzw6lBiMvYUg7GSUEB3rrpiNYg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/user/intellijideavideo)
[![PyCharm by JetBrains](https://yt3.ggpht.com/ytc/AKedOLQDA1klp1_3RHR-OF04DbnzSUxILe9uazyHsAjP=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCak6beUTLlVmf0E4AmnQkmw)
[![nodiagnosticrequired](https://yt3.ggpht.com/ytc/AKedOLTum-rqqyQ5acBWoAb2qe4lduyZ4TtBqne9HLc2=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCJZdS1wIqASD1MVrJyX8M2Q)
[![Kotlin by JetBrains](https://yt3.ggpht.com/QfykgcpEnn5dDj_yG8NwtcQiSiSSZuT091pzEACtBECGh7xuVydpV2l6rEw1IJB0kzLQaTKnfg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCP7uiEZIqci43m22KDl0sNw)
[![JetBrains Techlab](https://yt3.ggpht.com/f_aCqHDN3fXJ_T0HEvHyyRYoy4Z0zQ-whlVtXumhYrtRNMvZSL5bj9Mfj0y9iKxPFH--g6jP=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCU1w5Rx4bnDjw6kh3qFdTbg)


### *Scientific Channels*

[![anaHr](https://yt3.ggpht.com/ytc/AKedOLSmlT3YjOGm8ULjUARe8daZkRzom1zqT5KH6iKE=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/anaHr)
[![Khan Academy](https://yt3.ggpht.com/ytc/AKedOLTYTpeOiL8n_l9gepDh3m3vczvElfHZO-BR6oAf=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/khanacademy)

### *Politics and Economics Channels*

[![اقتصاد الكوكب](https://yt3.ggpht.com/ytc/AKedOLTjvlaBHO_gyhx9dtAajhJrb75u_Y--YtMhfrTTzg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCjMdgUQQM68S7tdXspE45Ag)
[![المخبر الاقتصادي - Mokhbir Eqtisadi](https://yt3.ggpht.com/ytc/AKedOLQ4Kib6pue0ajUUgXRJiMZI6dbZt7qgLVn3EXbB_g=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/MokhbirEqtisadi)
[![Ahmed El-Beheiry_أحمد بحيري](https://yt3.ggpht.com/ytc/AKedOLQsNi7da9g2qpuKXGzpqOOygUjWrtyILMv9aJo5=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/AhmedBehiry)
[![Alshakhanah Alshangy](https://yt3.ggpht.com/ytc/AKedOLSPRlknL8LrgAuFmCpqTDQZYXGmSiCuqWRlJNItAg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/AlshakhanahAlshangy)

### *FootBall & Others Channels*

[![ElKaweer - الكوير ](https://yt3.ggpht.com/ytc/AKedOLQXHr3-Z9hZiWcA7jUS_O4VpGD4l2CTpX9nveyiXw=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCvRbdoDaIeiY6umPJg1L0KA)
[![Mamdouh NasrAllah](https://yt3.ggpht.com/ytc/AKedOLQrZVs2tAtB3vHZyByEdT38Li-o_NI2okjGIFe3Bg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UC9c89meMU-ES5hb3CVAgIjQ)
[![MNEU](https://yt3.ggpht.com/Y04vRk57lxiqPaiz8SXglGX_dmAweusWyYfKVti4JWS45MJKcenBJZS-vTBDLMprewIKQkK4=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/user/NasrAllah99)
[![Omar Abdelrahim](https://yt3.ggpht.com/ytc/AKedOLR3JfcX0JwLPG3I2bV2D8i2xo1khQpzXFwFX4pM=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCxmEEGYXJkgJJO12nJhXl5g)
[![محمد عدنان Mohamad Adnan](https://yt3.ggpht.com/ytc/AKedOLSz8tiRdtqVlfndyVVxqJczA5qvz95fcBa22smdiQ=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/channel/UCxUPU7lI249SW_j5WgByJRA)
[![FC Bayern Munich](https://yt3.ggpht.com/bUoqRSR8mGhGwTX0rPgsFUNAaDM2yZacgYOoSYtuv21-GXOGeDehD5glMD3S50YOsGYIvgsMWA=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/fcbayern)
[![German Football](https://yt3.ggpht.com/ytc/AKedOLTKQ0Ujgoi92io1hNgN56iSvZmyM_bYCxTwqKy2=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/GermanFootball)
[![Bundesliga](https://yt3.ggpht.com/ytc/AKedOLQRRttxgURK4xqE0A-GcFZlEhxL5skaKlYLjZtOyg=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/bundesligatv)
[![beIN SPORTS](https://yt3.ggpht.com/ytc/AKedOLT3ruHgMaBrdKn9IJwUZQen-8BlZeOy4J_2zG4gOw=s88-c-k-c0x00ffffff-no-rj)](https://www.youtube.com/c/beINSPORTS/channels)



## Games & Hobbies :
Games :

![stronghold crusader](https://img.icons8.com/fluency/48/000000/crusader.png)
![valorant](https://img.icons8.com/color/48/000000/valorant.png)
![FIFA](https://img.icons8.com/color/48/000000/fifa18.png)
![pes](https://img.icons8.com/officel/16/000000/football2--v1.png)
![gta](https://img.icons8.com/ios-filled/50/000000/rockstar-games.png)
![gta-grand](https://img.icons8.com/dusk/50/000000/grand-theft-auto-v.png)
![Hulk](https://img.icons8.com/color/48/000000/hulk.png)

Hobbies : 

![music](https://img.icons8.com/fluency/48/000000/apple-music.png)
![football](https://img.icons8.com/ios/50/000000/goalkeeper-with-net.png)
![sport](https://img.icons8.com/color/48/000000/running--v1.png)
![wwe](https://img.icons8.com/color/48/000000/championship-belt.png)
![pc building](https://img.icons8.com/fluency/48/000000/pc-on-desk.png)


## Favorite National Team & Team :      

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp; **Germany 🇩🇪**  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;**FC Bayern München ❤️❤️**

<p align="center">
    <img height="165" src="/Images/dfb_team.png" alt="Germany 🇩" />
       &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
    <img height="135" src="/Images/fcbayern.png" alt="FC Bayern München ❤️❤️" />
</p>
